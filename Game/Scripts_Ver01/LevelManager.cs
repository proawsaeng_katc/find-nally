﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;

    [SerializeField] private float timeLimit = 0;                       //เวลานับถอยหลัง
    [SerializeField] private int maxHiddenObjectToFound = 6;            //maximum hidden objects ในแต่ละ level
    [SerializeField] private ObjectHolder objectHolderPrefab;           //Prefabลิสต์ของทั้งหมด

    [HideInInspector] public GameStatus gameStatus = GameStatus.NEXT;   //keep track of Game Status
    private List<HiddenObjectData> activeHiddenObjectList;              //ลิสต์ไอเทมที่ต้องหา
    private float currentTime;                                          //เวลาที่เหลือ
    private int totalHiddenObjectsFound = 0;                            //ไอเทมที่เจอ
    private TimeSpan time;                                              
    private RaycastHit2D hit;
    private Vector3 pos;                                                

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        activeHiddenObjectList = new List<HiddenObjectData>();          
        AssignHiddenObjects();                                  
    }

    void AssignHiddenObjects()  //Method กำหนดไอเทมที่ต้องหา
    {
        ObjectHolder objectHolder = Instantiate(objectHolderPrefab, Vector3.zero, Quaternion.identity);
        objectHolderPrefab.gameObject.SetActive(false);
        totalHiddenObjectsFound = 0;                                        
        activeHiddenObjectList.Clear();                                    
        gameStatus = GameStatus.PLAYING;                                    
        UIManager.instance.TimerText.text = "" + timeLimit;                 
        currentTime = timeLimit;                                            

        for (int i = 0; i < objectHolder.HiddenObjectList.Count; i++)       
        {
            //deacivate all collider
            objectHolder.HiddenObjectList[i].hiddenObj.GetComponent<Collider2D>().enabled = false; 
        }

        int k = 0; 
        while (k < maxHiddenObjectToFound)
        {
            int randomNo = UnityEngine.Random.Range(0, objectHolder.HiddenObjectList.Count);
            if (!objectHolder.HiddenObjectList[randomNo].makeHidden)
            {
                objectHolder.HiddenObjectList[randomNo].hiddenObj.name = "" + k;    //set their name to index because we use index to identify tapped object

                objectHolder.HiddenObjectList[randomNo].makeHidden = true;          //เซทไอเทมที่ต้องหา
                                                                                    
                objectHolder.HiddenObjectList[randomNo].hiddenObj.GetComponent<Collider2D>().enabled = true;   //activate collider
                activeHiddenObjectList.Add(objectHolder.HiddenObjectList[randomNo]);   //เพิ่มลิสต์ไอเทมที่ต้องหา
                k++;                                                                
            }
        }

        UIManager.instance.PopulateHiddenObjectIcons(activeHiddenObjectList);   
        gameStatus = GameStatus.PLAYING;                                        
    }

    private void Update()
    {
        if (gameStatus == GameStatus.PLAYING)                               
        {
            if (Input.GetMouseButtonDown(0))                                
            {
                pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);  
                hit = Physics2D.Raycast(pos, Vector3.zero);                 
                if (hit && hit.collider != null)                            
                {
                    hit.collider.gameObject.SetActive(false);               //deactivate collider that clicked
                    
                    UIManager.instance.CheckSelectedHiddenObject(hit.collider.gameObject.name); //send the name of hit object to UIManager

                    for (int i = 0; i < activeHiddenObjectList.Count; i++)
                    {
                        if (activeHiddenObjectList[i].hiddenObj.name == hit.collider.gameObject.name)
                        {
                            activeHiddenObjectList.RemoveAt(i);
                            break;
                        }
                    }

                    totalHiddenObjectsFound++;                              
                    
                    
                    if (totalHiddenObjectsFound >= maxHiddenObjectToFound)  
                    {
                        Debug.Log("Level Complete");                      
                        UIManager.instance.GameCompleteObj.SetActive(true); 
                        gameStatus = GameStatus.NEXT;                       
                    }
                }
            }

            currentTime -= Time.deltaTime;  

            time = TimeSpan.FromSeconds(currentTime);                       
            UIManager.instance.TimerText.text = time.ToString("mm':'ss");   
            if (currentTime <= 0)                                           
            {
                Debug.Log("Time Up");                                       
                UIManager.instance.TimeUpObj.SetActive(true);         
                gameStatus = GameStatus.NEXT;                               
            }
        }
    }

    public IEnumerator HintObject() 
    {
        int randomValue = UnityEngine.Random.Range(0, activeHiddenObjectList.Count);
        Vector3 originalScale = activeHiddenObjectList[randomValue].hiddenObj.transform.localScale;
        activeHiddenObjectList[randomValue].hiddenObj.transform.localScale = originalScale * 1.25f;
        yield return new WaitForSeconds(0.25f);
        activeHiddenObjectList[randomValue].hiddenObj.transform.localScale = originalScale;
    }
}

[System.Serializable]
public class HiddenObjectData
{
    public string name;
    public GameObject hiddenObj;
    public bool makeHidden = false;
}

public enum GameStatus
{
    PLAYING,
    NEXT
}
